/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');


//why cons = cons = ?
var cons = cons = require('consolidate');

//moment lib for parsing and comparing the date and time, it looks like very powerful lib
var moment = require('moment');
//moment().format("dddd, MMMM Do YYYY, h:mm:ss a");

var app = express();


//=========================Steven's Code===========================================
//Set up redis as a requirement, rest of redis code is in api
var redis = require("redis");
var redisClient = getRedisClient(redis);
//=========================End of Steven's Code====================================

//var timeTosent;

//=================================================================================
//global variable to be sent, created here for testing, later should be tested
var timeTosend; // later to be filled with unix time epoch in ms

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
//app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/public/images', express.static(path.join(__dirname, '/public/images')));
// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

// assign the swig engine to .html files
app.engine('html', cons.swig);

app.get('/', routes.index);
app.get('/reminder', routes.reminder);
app.get('/users', user.list);

app.post('/Send', function (req, res) {
    // Specifies which URL to listen for
    // req.body -- contains form data
    // Your accountSid and authToken from twilio.com/user/account

    var date = new Date();

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var currentTime = year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

    var replaceString = req.body.date.replace("T", " ")
    var msg = "Hello " + req.body.fullName + ", \n You have set up an appointment"
        + " at " +replaceString+"\n With the notes : " + req.body.notes;



    //=================================================================================
    //beginning of reminder evalution
    //insert the datetime in unix epoch format in redis



    //get the select value in the request
    var selectValue =  req.body.reminderTime;
    //selected date from calendar
    var reqDate = req.body.date;
    console.log ('date: ' + reqDate + ' selected reminder time ' + selectValue);



    //convert select value to int
    var int_selectValue = parseInt(selectValue);

    //get the offset value to be subtracted from the selected time in miliseconds
    switch (int_selectValue)
    {
        case 1:
            var diffInMS = 900000;      //15 mins
            break;
        case 2:
            var diffInMS = 1800000;     //30 mins
            break;
        case 3:
            var diffInMS = 3600000;     //1 hr
            break;
        case 4:
            var diffInMS = 7200000;     //2 hrs
            break;
        case 5:
            var diffInMS = 10800000;    //3 hrs
            break;
        case 6:
            var diffInMS = 21600000;    //6 hrs
            break;
        case 7:
            var diffInMS = 43200000;    //12 hrs
            break;
        case 8:
            var diffInMS = 86400000;    //1 day
            break;
        case 9:
            var diffInMS = 172800000;   //2 days
            break;
        default:
            console.log('no option was chosen!!!!');

    }







    //get the selected date, format: unix epoch time in miliseconds
    var selectDate = Date.parse(reqDate);

    //DEBUG
    console.log('selected Date in unix foramt' + selectDate);


    //the date and time that the message should be sent, format: unix epoch time in miliseconds
    timeTosend = selectDate - diffInMS;


    //DEBUG, converting the opoch miliseconds to JS date object
    var dateString = new Date(selectDate);
    var sentDateString = new Date(timeTosend);


    //will display time selected in UTC format for debugging and future use
    var formattedTime = dateString.toUTCString();

    //will display the reminder due date in UTC format string for debugging and future use
    var formattedTimeSend = sentDateString.toUTCString();


    //DEBUG
    console.log('selected date is: ' + formattedTime + ' time to send the sms: ' + formattedTimeSend);


    //End of my change in here
    //=================================================================================

    //=================================Add to Redis====================================
    //TODO
    //insert the following key values to the redis or db
    //=========================Steven's Code===========================================
    addReminderToRedis(timeTosend, "Put contact Id here :" + timeTosend);
    //=========================End of Steven's Code====================================
    //user contact info
    //user selected date
    //delivery method
    //reminder date and time in unix epoch format



    if(req.body.cbPhone == "on") {
       sendSMS(req, msg, currentTime);
   }
    if(req.body.cbEmail === "on") {
        sendEmail(req, msg, currentTime);
    }

    res.writeHead(200, {
        "Content-Type": "text/html"
      });
    res.end("<!doctype html><html lang=\"en\"><head><meta charset=\"utf-8\"><title>Confirmation</title></head><body ><h4>Message Sent</h4></body></html>");
});

var port = process.env.PORT || 5000;
http.createServer(app).listen(port, function () {
    console.log('Express server listening on port ' + app.get('port'));
});


var accountSid = 'AC97d4d99a4be4f16c3e283dc7921eb129';
var authToken = "8b7d5fb14e891bb872243bafd45b8a66";
var client = require('twilio')(accountSid, authToken);


var logentries = require('node-logentries');
var log = logentries.logger({
    token:'d50e39f9-ee9d-4212-b5f7-f9385582559c'
});


//call the isItTheTime function every min, this is async, and internally uses a queue
var checkReminderTimeIndb = setInterval(function(){isItTheTime()}, 6000);

function sendSMS(req, msg, currentTime){
    var accountSid = 'AC97d4d99a4be4f16c3e283dc7921eb129';
    var authToken = "8b7d5fb14e891bb872243bafd45b8a66";
    var client = require('twilio')(accountSid, authToken);

    client.sms.messages.create({
        body: msg,
        to: req.body.phoneNumber,
        from: "+18583467033"
    }, function (err, message) {
        process.stdout.write(message.sid);
    });
    // level specific methods like 'info', 'debug', etc.
    log.info("Text Message is sent " + currentTime )

}
function sendEmail(req, msg, currentTime){
    var Mailgun = require('mailgun').Mailgun;
    var mg = new Mailgun('key-0q534y4nvmcy9jxaufq7cl72lkzz0527');

    mg.sendText('Samantha@techieasians.mailgun.org',req.body.email,
        'Techie Asians Reminder!',
        msg,
        'Samantha@techieasians.mailgun.org', {},
        function(err) {
            if (err) console.log('Oh noes 3: ' + err);
            else     console.log('Success');
        });

    log.info("Email Message is sent " + currentTime)
}


//=================================================================================
//interval based checking
//alternative event based checking, needs a library module like node-schedule
//or seprating this and assign it to a worker, declate in Procfile and set in heroku



//to just do it once - for debug puposes in here
var alreadySend = false;

//all the db accesses, checking logic, and the calls to the sendSMS and sendEmail will happen in this
function isItTheTime() {

    //to adjust for time zone - patched, moment.js may give a better option
    var gmtZoneAdjust = 28800000;

    // getting the current time in ms and converting in to int, and offseting it from gmt offset
    var curTimeInMS = (parseInt((new Date).getTime())) - gmtZoneAdjust;
    console.log('INSIDE isItTheTime() :: current time: ' + curTimeInMS + ', time to send is set to: ' + timeTosend);


    // if curTime in ms is greater than timeToSend and already send is false then call sendSMS and sendEmail
    //this is prelemintary, we need a better logic for this

    if (timeTosend === undefined) {
        console.log('timeTosend is not yet redi');
    }
    else if ( (curTimeInMS > timeTosend) && (alreadySend == false) ) {
        //should call the sendSMS and sendEmail
        console.log('sendSMS and sendEmail is called');
        alreadySend = true;
    }
    else { console.log('just checking... :)'); }
    
    //=========================Steven's Code===========================================
    //Now retrevie the reminder pairs from redis
    //Returns a list of contact id, in which maps to the corresponding id in mongodb
    var contactIdArr = getReminderFromRedis(curTimeInMS);
    //=========================End of Steven's Code====================================
    //loop the arr like this
    /*
        for (var i = 0; i < contactIdArr.length; i++) {
			console.log('String : ', contactIdArr[i]);
		}
    */
    //Need to get info from mongodb so we can do sendSMS and sendEmail

    //put them in the array, and then call the sendSMS and sendEmail function for each of them
}
//=================================================================================

//==============================Redis API Code=====================================
//Use for debugging, make connection to local redis server 
function getRedisClientLocal(redis){
    redisClient = redis.createClient();
    redisClient.on("error", function (err) {
        console.log("Error " + err);
    });
    console.log("Successfully connect to redis local server!");
    return redisClient;
}

//Make connection to redis server
//Return the client obj
function getRedisClient(redis){

    var port = 11001;
    var host = "beardfish.redistogo.com";
    var username = "redistogo";
    var password = "b8c4dd4b66d214245f4c4e420d00d472";
    
    var redisClient = redis.createClient(port, host); 
    redisClient.auth(password, function() {
        console.log("Succefully connected to redis remote server!");
    });
    
    return redisClient;
}

//Add a reminder pair <timestamp, contact_id> to redis
//Internally pair is stored in a sorted set called "reminder_set" 
function addReminderToRedis( timestamp, contact_id){
    var args = ['reminder_set', timestamp, contact_id ];  
    redisClient.zadd(args, function (err, response) {
        if (err) throw err;
        console.log('added '+response+' items.');
    });
}

//Get and Pop the array of reminder pairs from the set
//Return the result in a array of contact_id
function getReminderFromRedis( currTimestamp){
    var args = [ 'reminder_set', '0', currTimestamp ];
    var resultArr;
    //Get the list of pairs from the set 
    redisClient.zrangebyscore(args, function (err, response) {
        if (err) throw err;
   
        console.log('Performed request : ', response);
        resultArr = response;
    });
    //Remove the pairs from the set
    redisClient.zremrangebyscore(args, function (err, response) {
        if (err) throw err;
        console.log('list successfully removed', response);
    }); 
	return resultArr;
}

function shutDownRedisClient(){
    redisClient.quit();
}
